/**
 * 
 */
package ro.tuc.pt.tema3.presentation;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * @author Vali
 *
 */
public class MainPanel extends JPanel {
	private static final long serialVersionUID = -7010858473355030619L;
	
	public MainPanel(final App window) {
		this.setLayout(new GridLayout(0, 1));
		
		JLabel welcome = new JLabel("Welcome, worker!");
		welcome.setFont(new Font("Ubuntu", Font.PLAIN, 22));
		welcome.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(welcome);

		JPanel panel = new JPanel(new FlowLayout());
		
		JButton btnNewClient = new JButton("Add new client");
		btnNewClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.refreshMiddle(MiddleState.CLIENTS_NEW);				
			}			
		});		
		panel.add(btnNewClient);
		
		JButton btnEditClient = new JButton("Edit client");
		btnEditClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.refreshMiddle(MiddleState.CLIENTS_EDIT);				
			}			
		});		
		panel.add(btnEditClient);
		
		JButton btnDeleteClient = new JButton("Delete client");
		btnDeleteClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.refreshMiddle(MiddleState.CLIENTS_DELETE);				
			}			
		});		
		panel.add(btnDeleteClient);

		JButton btnViewAllClients = new JButton("View all clients");
		btnViewAllClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.refreshMiddle(MiddleState.CLIENTS_VIEWALL);				
			}			
		});		
		panel.add(btnViewAllClients);
		
		JButton btnNewProduct = new JButton("Add new product");
		btnNewProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.refreshMiddle(MiddleState.PRODUCTS_NEW);				
			}			
		});		
		panel.add(btnNewProduct);
		
		JButton btnEditProduct = new JButton("Edit product");
		btnEditProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.refreshMiddle(MiddleState.PRODUCTS_EDIT);				
			}			
		});		
		panel.add(btnEditProduct);
		
		JButton btnDeleteProduct = new JButton("Delete product");
		btnDeleteProduct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.refreshMiddle(MiddleState.PRODUCTS_DELETE);				
			}			
		});		
		panel.add(btnDeleteProduct);

		JButton btnViewAllProducts = new JButton("View all products");
		btnViewAllProducts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.refreshMiddle(MiddleState.PRODUCTS_VIEWALL);				
			}			
		});		
		panel.add(btnViewAllProducts);
		
		JButton btnNewOrder = new JButton("New Order");
		btnNewOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				window.refreshMiddle(MiddleState.NEW_ORDER);				
			}			
		});		
		panel.add(btnNewOrder);
		
		this.add(panel);
	}
}
