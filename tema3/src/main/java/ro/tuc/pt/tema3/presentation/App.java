package ro.tuc.pt.tema3.presentation;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;


/**
 * @author Vali
 */
public class App extends JFrame {
	private static final long serialVersionUID = -6098290837278828452L;
	private JPanel centerPanel;
	
	public App() {
		this.setTitle("Warehouse Order Management");
		this.setSize(1300, 720);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setLayout(new BorderLayout());
		
		/**
		 * Initializing centerPanel
		 */
		this.centerPanel = new MainPanel(this);
		
		
		/**
		 * Header description
		 */
		JPanel header = new JPanel();
		header.setLayout(new BoxLayout(header, BoxLayout.Y_AXIS));
		
		JLabel title = new JLabel("Warehouse Order Management");
		title.setFont(new Font("Ubuntu", Font.PLAIN, 32));
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		header.add(title);
		header.add(new JSeparator());
		this.add(header, BorderLayout.PAGE_START);
			
		/**
		 * Buttons panel
		 */
		JMenuBar mbar = new JMenuBar();
		this.setJMenuBar(mbar);
		
		JMenu mclients = new JMenu("Clients");
		JMenuItem mcnew = new JMenuItem("New");
		mcnew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshMiddle(MiddleState.CLIENTS_NEW);				
			}			
		});
		mclients.add(mcnew);
		JMenuItem mcedit = new JMenuItem("Edit");
		mcedit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshMiddle(MiddleState.CLIENTS_EDIT);				
			}			
		});
		mclients.add(mcedit);
		JMenuItem mcdelete = new JMenuItem("Delete");
		mcdelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshMiddle(MiddleState.CLIENTS_DELETE);				
			}			
		});
		mclients.add(mcdelete);
		JMenuItem mcviewall = new JMenuItem("View all");
		mcviewall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshMiddle(MiddleState.CLIENTS_VIEWALL);				
			}			
		});
		mclients.add(mcviewall);
		mbar.add(mclients);
		
		JMenu mproducts = new JMenu("Products");
		JMenuItem mpnew = new JMenuItem("New");
		mpnew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshMiddle(MiddleState.PRODUCTS_NEW);				
			}			
		});
		mproducts.add(mpnew);
		JMenuItem mpedit = new JMenuItem("Edit");
		mpedit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshMiddle(MiddleState.PRODUCTS_EDIT);				
			}			
		});
		mproducts.add(mpedit);
		JMenuItem mpdelete = new JMenuItem("Delete");
		mpdelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshMiddle(MiddleState.PRODUCTS_DELETE);				
			}			
		});
		mproducts.add(mpdelete);
		JMenuItem mpviewall = new JMenuItem("View all");
		mpviewall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshMiddle(MiddleState.PRODUCTS_VIEWALL);				
			}			
		});
		mproducts.add(mpviewall);
		mbar.add(mproducts);
		
		JMenu mneworder = new JMenu("New order");
		mneworder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				refreshMiddle(MiddleState.NEW_ORDER);				
			}			
		});
		mbar.add(mneworder);
		
		mbar.add(Box.createHorizontalGlue());
		
		JMenu mhome = new JMenu("Home");
		mhome.addMenuListener(new MenuListener() {
			public void menuCanceled(MenuEvent arg0) { }

			public void menuDeselected(MenuEvent arg0) { }

			public void menuSelected(MenuEvent arg0) {
				refreshMiddle(MiddleState.MAIN_MENU);
			}
		});
		mbar.add(mhome);
		
		JMenu mexit = new JMenu("Exit");
		mexit.addMenuListener(new MenuListener() {
			public void menuCanceled(MenuEvent arg0) { }

			public void menuDeselected(MenuEvent arg0) { }

			public void menuSelected(MenuEvent arg0) {
				dispose();
			}
		});
		mbar.add(mexit);
		
		/**
		 * Center panel
		 */
		this.add(centerPanel, BorderLayout.CENTER);
		
		/**
		 * Footer description
		 */
		JPanel footer = new JPanel();
		footer.setLayout(new BoxLayout(footer, BoxLayout.Y_AXIS));

		footer.add(new JSeparator());
		JLabel credits = new JLabel("Tema 3 Tehnici de Programare - Valentin Deaconu");
		credits.setFont(new Font("Ubuntu", Font.PLAIN, 16));
		credits.setAlignmentX(Component.CENTER_ALIGNMENT);
		footer.add(credits);
		
		this.add(footer,  BorderLayout.PAGE_END);
	}
	
	public void refreshMiddle(MiddleState state) {
		this.remove(centerPanel);
		switch (state) {
			case CLIENTS_NEW:
				this.centerPanel = new ClientPanel(0);
				break;
			case CLIENTS_EDIT:
				this.centerPanel = new ClientPanel(1);
				break;
			case CLIENTS_DELETE:
				this.centerPanel = new ClientPanel(2);
				break;
			case CLIENTS_VIEWALL:
				this.centerPanel = new ClientPanel(3);
				break;
			case PRODUCTS_NEW:
				this.centerPanel = new ProductPanel(0);
				break;
			case PRODUCTS_EDIT:
				this.centerPanel = new ProductPanel(1);
				break;
			case PRODUCTS_DELETE:
				this.centerPanel = new ProductPanel(2);
				break;
			case PRODUCTS_VIEWALL:
				this.centerPanel = new ProductPanel(3);
				break;
			case NEW_ORDER:
				this.centerPanel = new OrderPanel();
				break;
			case MAIN_MENU:
			default:
				this.centerPanel = new MainPanel(this);
				break;
		}
		this.add(centerPanel, BorderLayout.CENTER);
		
		this.revalidate();
		this.repaint();
	}
	
	public static void main( String[] args ) {
        App window = new App();
        window.setVisible(true);
    }
}
