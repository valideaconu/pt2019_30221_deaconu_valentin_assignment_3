/**
 * Order class used to interact with orders and packages tables in database
 */
package ro.tuc.pt.tema3.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javafx.util.Pair;
import ro.tuc.pt.tema3.dataAccessLayer.Database;

/**
 * @author Vali
 *
 */
public class Order {
	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	
	private int id;
	private int clientId;
	private String date;
	private ArrayList< Pair<Product, Integer> > products;

	private Order(int id, int clientId, String date, ArrayList< Pair<Product, Integer> > products) {
		this.id = id;
		this.clientId = clientId;
		this.products = products;
		this.date = date;
	}
	
	public Order(int id, int clientId, ArrayList< Pair<Product, Integer> > products) {
		this(id, clientId, dateFormat.format(Calendar.getInstance()), products);
	}
	
	public Order(int clientId, ArrayList< Pair<Product, Integer> > products) {
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the clientId
	 */
	public int getClientId() {
		return clientId;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @return the products
	 */
	public ArrayList< Pair<Product, Integer> > getProducts() {
		return products;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Order [id=" + id + ", clientId=" + clientId + ", date=" + date + ", products=" + products + "]";
	}

	/**
	 * Method used to download all products that are assigned to an order
	 * @param orderId the id of order
	 * @return the products list
	 */
	private static ArrayList< Pair<Product, Integer> > getOrderProducts(int orderId) {
		String sql = "SELECT product_id, quantity FROM packages WHERE order_id = '" + orderId + "';";
		
		ArrayList< ArrayList<Object> > results = Database.doQuery(sql);
		ArrayList< Pair<Product, Integer> > products = new ArrayList< Pair<Product, Integer> >();
		
		if (results != null) {
			if (results.size() > 0) {
				for (ArrayList<Object> res : results) {
					int productId = ((Long) res.get(0)).intValue();
					int quantity = ((Long) res.get(1)).intValue();
					
					Pair<Product, Integer> pair = new Pair<Product, Integer>(
							Product.download(productId), 
							quantity
						);
					
					products.add(pair);
				}
				
				return products;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	/**
	 * Method for extracting an order from database using its id
	 * @param id the id of order
	 * @return the order
	 */
	public static Order download(int id) {
		String sql = "SELECT client_id, date FROM orders WHERE id = " + id + ";";
		ArrayList< ArrayList<Object> > results = Database.doQuery(sql);
		
		if (results != null) {
			if (results.size() == 1) {
				ArrayList<Object> res = results.get(0);
				
				return new Order(
						id,
						((Long) res.get(0)).intValue(),
						((String) res.get(1)),
						Order.getOrderProducts(id)
					);
				
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	/**
	 * Method for uploading a new order to database
	 * @param o the order object
	 * @return true if operation succeeded, else false
	 */
	public static boolean insert(Order o) {
		String sql = "INSERT INTO orders (client_id, date) VALUES ("
				+ "'" + o.clientId + "', '" + o.date + "');";
		
		if (Database.doInsert(sql) != 1)
			return false;
		
		for (Pair<Product, Integer> pair : o.products) {
			sql = "INSERT INTO packages (order_id, product_id, quality) VALUES ("
					+ "'" + o.id + "', '" + pair.getKey().getId() + "', '" + pair.getValue() + "');";
			
			if (Database.doInsert(sql) != 1)
				return false;
		}
		
		return true;
	}
}

