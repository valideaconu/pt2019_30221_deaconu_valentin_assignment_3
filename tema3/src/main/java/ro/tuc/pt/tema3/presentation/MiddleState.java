/**
 * 
 */
package ro.tuc.pt.tema3.presentation;

/**
 * @author Vali
 *
 */
public enum MiddleState {
	CLIENTS_NEW, 
	CLIENTS_EDIT, 
	CLIENTS_DELETE, 
	CLIENTS_VIEWALL,
	PRODUCTS_NEW, 
	PRODUCTS_EDIT, 
	PRODUCTS_DELETE, 
	PRODUCTS_VIEWALL,
	NEW_ORDER,
	MAIN_MENU
}
