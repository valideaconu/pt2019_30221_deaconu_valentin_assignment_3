/**
 * Client class used to interact with clients table in database
 */
package ro.tuc.pt.tema3.model;

import java.util.ArrayList;


import ro.tuc.pt.tema3.dataAccessLayer.Database;

/**
 * @author Vali
 *
 */
public class Client {
	private int id;
	private String name;
	private String adress;
	private String phone;
	
	public Client(int id, String name, String adress, String phone) {
		this.id = id;
		this.name = name;
		this.adress = adress;
		this.phone = phone;
	}

	public Client() { }

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 * @return true if operation succeeded, else false
	 */
	public boolean setName(String name) {
		if (name.matches("[a-zA-Z]+")) {
			this.name = name;
			return true;
		}
		
		return false;
	}

	/**
	 * @return the adress
	 */
	public String getAdress() {
		return adress;
	}

	/**
	 * @param adress the adress to set
	 * @return true if operation succeeded, else false
	 */
	public boolean setAdress(String adress) {
		for (int i = 0; i < adress.length(); ++i) {
			char c = adress.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c) && 
					!(c == ' ') && !(c == '.') && !(c == ',')) {
				return false;
			}
		}
		
		this.adress = adress;
		return true;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 * @return true if operation succeeded, else false
	 */
	public boolean setPhone(String phone) {
		for (int i = 0; i < phone.length(); ++i) {
			char c = phone.charAt(i);
			if (!Character.isDigit(c) && !(c == '+')) {
				return false;
			}
		}

		this.phone = phone;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Client [id=" + id + ", name=" + name + ", adress=" + adress + ", phone=" + phone + "]";
	}

	/**
	 * Method for extracting a client from database using his id
	 * @param id the id of the client
	 * @return the client
	 */
	public static Client download(int id) {
		String sql = "SELECT name, adress, phone FROM clients WHERE clients.id = " + id + ";";
		ArrayList< ArrayList<Object> > results = Database.doQuery(sql);
		
		if (results != null) {
			if (results.size() == 1) {
				ArrayList<Object> res = results.get(0);
				return new Client(
						id,
						(String) res.get(0),
						(String) res.get(1),
						(String) res.get(2)
					);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	/**
	 * Method for uploading a new client to database
	 * @param c the client object
	 * @return true if operation succeeded, else false
	 */
	public static boolean insert(Client c) {
		String sql = "INSERT INTO clients (name, adress, phone) VALUES ("
				+ "'" + c.name + "', '" + c.adress + "', '" + c.phone + "');";
		
		if (Database.doInsert(sql) == 1)
			return true;
		
		return false;
	}

	/**
	 * Method for updating an existing client on database using his id
	 * @param c the client object
	 * @return true if operation succeeded, else false
	 */
	public static boolean update(Client c) {
		String sql = "UPDATE clients "
				+ "SET name = '" + c.name + "', adress = '" + c.adress + "', phone = '" + c.phone + "' "
				+ "WHERE id = '" + c.id + "';";
		
		if (Database.doUpdate(sql) == 1)
			return true;
		
		return false;
	}
	
	/**
	 * Method for deleting an existing client on database using his id
	 * @param c the client object
	 * @return true if operation succeeded, else false
	 */
	public static boolean delete(Client c) {
		String sql = "DELETE FROM clients WHERE id = '" + c.id + "';";
		
		if (Database.doUpdate(sql) == 1)
			return true;
		
		return false;
	}
	
	/**
	 * Method for extracting all clients from database
	 * @return a list with all clients
	 */
	public static ArrayList<Client> downloadAll() {
		String sql = "SELECT id, name, adress, phone FROM clients;";
		ArrayList< ArrayList<Object> > results = Database.doQuery(sql);
		
		if (results != null) {		
			ArrayList<Client> clients = new ArrayList<Client>();
			
			for(ArrayList<Object> arr : results) {
				clients.add(new Client(
						((Long) arr.get(0)).intValue(),
						(String) arr.get(1),
						(String) arr.get(2),
						(String) arr.get(3)
					));
			}
			
			return clients;
		} else {
			return null;
		}
	}
}
