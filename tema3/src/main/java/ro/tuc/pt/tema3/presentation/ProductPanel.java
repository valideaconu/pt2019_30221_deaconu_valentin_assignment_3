/**
 * 
 */
package ro.tuc.pt.tema3.presentation;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import ro.tuc.pt.tema3.businessLayer.TableGenerator;
import ro.tuc.pt.tema3.model.Product;

/**
 * @author Vali
 *
 */
public class ProductPanel extends JPanel {
	private static final long serialVersionUID = 6547606762582495368L;

	public ProductPanel(int model) {
		switch (model) {
			case 0:
				// New client
				initNew();
				break;
			case 1:
				// Edit client
				initEdit();
				break;
			case 2:
				// Delete client
				initDelete();
				break;
			case 3:
			default:
				// View all clients
				initViewAll();
				break;
		}
	}
	
	private void initNew() {		
		JPanel inside = new JPanel();
		
		SpringLayout layout = new SpringLayout();
		inside.setLayout(layout);
		
		JLabel lblName = new JLabel("Name:", JLabel.TRAILING);
		lblName.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblName.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblName);
		
		final JTextField tfName = new JTextField(40);
		lblName.setLabelFor(tfName);
		tfName.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		inside.add(tfName);
		
		JLabel lblDescription = new JLabel("Description:", JLabel.TRAILING);
		lblDescription.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblDescription.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblDescription);
		
		final JTextField tfDescription = new JTextField(40);
		lblDescription.setLabelFor(tfDescription);
		tfDescription.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		inside.add(tfDescription);
		
		JLabel lblPrice = new JLabel("Price:", JLabel.TRAILING);
		lblPrice.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblPrice.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblPrice);
		
		final JTextField tfPrice = new JTextField(40);
		tfPrice.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblPrice.setLabelFor(tfPrice);
		inside.add(tfPrice);
		
		JLabel lblQuantity = new JLabel("Quantity:", JLabel.TRAILING);
		lblQuantity.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblQuantity.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblQuantity);
		
		final JTextField tfQuantity = new JTextField(40);
		tfQuantity.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblQuantity.setLabelFor(tfQuantity);
		inside.add(tfQuantity);
		
		SpringUtilities.makeCompactGrid(inside, 4, 2, 40, 40, 80, 80);
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(Box.createVerticalGlue());
		this.add(inside);
		this.add(Box.createVerticalGlue());

		JButton btnSend = new JButton("Send");
		btnSend.setFont(new Font("Ubuntu", Font.PLAIN, 16));
		btnSend.setHorizontalAlignment(SwingConstants.CENTER);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = tfName.getText();
				String description = tfDescription.getText();
				double price = 0.0;
				int quantity = 0;
				try {
					price = Double.parseDouble(tfPrice.getText());
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(
							null, 
							"Invalid fields.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				try {
					quantity = Integer.parseInt(tfQuantity.getText());
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(
							null, 
							"Invalid fields.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				Product p = new Product();
				if (p.setName(name) && p.setDescription(description) && p.setPrice(price) && p.setQuantity(quantity)) {
					if (Product.insert(p)) {
						JOptionPane.showMessageDialog(
								null, 
								"Product added successfully.",
								"Success",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(
								null, 
								"Something went wrong. Try again later.",
								"Error",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(
							null, 
							"Invalid fields.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}			
		});
		this.add(btnSend);
		
		this.add(Box.createVerticalGlue());
		
	}
	
	private void initEdit() {		
		JPanel inside = new JPanel();
		
		SpringLayout layout = new SpringLayout();
		inside.setLayout(layout);
		
		final ArrayList<Product> products = Product.downloadAll();
		final JComboBox<String> combo = new JComboBox<String>();
		combo.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		combo.setMaximumSize(new Dimension(400, 80));
		combo.setModel(getComboModel());

		JLabel lblName = new JLabel("Name:", JLabel.TRAILING);
		lblName.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblName.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblName);
		
		final JTextField tfName = new JTextField(40);
		lblName.setLabelFor(tfName);
		tfName.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		tfName.setText(products.get(0).getName());
		inside.add(tfName);
		
		JLabel lblDescription = new JLabel("Description:", JLabel.TRAILING);
		lblDescription.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblDescription.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblDescription);
		
		final JTextField tfDescription = new JTextField(40);
		lblDescription.setLabelFor(tfDescription);
		tfDescription.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		tfDescription.setText(products.get(0).getDescription());
		inside.add(tfDescription);
		
		JLabel lblPrice = new JLabel("Price:", JLabel.TRAILING);
		lblPrice.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblPrice.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblPrice);
		
		final JTextField tfPrice = new JTextField(40);
		tfPrice.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		tfPrice.setText(products.get(0).getPrice() + "");
		lblPrice.setLabelFor(tfPrice);
		inside.add(tfPrice);
		
		JLabel lblQuantity = new JLabel("Quantity:", JLabel.TRAILING);
		lblQuantity.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblQuantity.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblQuantity);
		
		final JTextField tfQuantity = new JTextField(40);
		tfQuantity.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		tfQuantity.setText(products.get(0).getQuantity() + "");
		lblQuantity.setLabelFor(tfQuantity);
		inside.add(tfQuantity);
		
		SpringUtilities.makeCompactGrid(inside, 4, 2, 40, 40, 80, 80);
		
		combo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				int index = combo.getSelectedIndex();
				Product p = products.get(index);
				tfName.setText(p.getName());
				tfDescription.setText(p.getDescription());
				tfPrice.setText(p.getPrice() + "");
				tfQuantity.setText(p.getQuantity() + "");
			}			
		});
		
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(Box.createVerticalGlue());
		this.add(combo);
		this.add(Box.createVerticalGlue());
		this.add(inside);
		this.add(Box.createVerticalGlue());

		JButton btnSend = new JButton("Send");
		btnSend.setFont(new Font("Ubuntu", Font.PLAIN, 16));
		btnSend.setHorizontalAlignment(SwingConstants.CENTER);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = tfName.getText();
				String description = tfDescription.getText();
				double price = 0.0;
				int quantity = 0;
				try {
					price = Double.parseDouble(tfPrice.getText());
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(
							null, 
							"Invalid fields.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				try {
					quantity = Integer.parseInt(tfQuantity.getText());
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(
							null, 
							"Invalid fields.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
					return;
				}
				
				Product p = products.get(combo.getSelectedIndex());
				if (p.setName(name) && p.setDescription(description) && p.setPrice(price) && p.setQuantity(quantity)) {
					if (Product.update(p)) {
						JOptionPane.showMessageDialog(
								null, 
								"Product updated successfully.",
								"Success",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(
								null, 
								"Something went wrong. Try again later.",
								"Error",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(
							null, 
							"Invalid fields.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}			
		});
		this.add(btnSend);
		
		this.add(Box.createVerticalGlue());
		
	}
	
	private void initDelete() {	
		JPanel inside = new JPanel();
		
		inside.setLayout(new FlowLayout());
		
		final JComboBox<String> combo = new JComboBox<String>();
		combo.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		combo.setMaximumSize(new Dimension(400, 80));
		combo.setModel(getComboModel());
		
		inside.add(combo);
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(Box.createVerticalGlue());
		this.add(inside);
		this.add(Box.createVerticalGlue());

		JButton btnSend = new JButton("Send");
		btnSend.setFont(new Font("Ubuntu", Font.PLAIN, 16));
		btnSend.setHorizontalAlignment(SwingConstants.CENTER);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Product p = Product.downloadAll().get(combo.getSelectedIndex());
				if (Product.delete(p)) {
					JOptionPane.showMessageDialog(
							null, 
							"Product deleted successfully.",
							"Success",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(
							null, 
							"Something went wrong. Try again later.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
				
				combo.setModel(getComboModel());
				combo.setSelectedIndex(0);
			}			
		});
		
		JPanel inside2 = new JPanel(new FlowLayout());
		inside2.add(btnSend);
		this.add(inside2);
		
		this.add(Box.createVerticalGlue());
		
	}
	
	private void initViewAll() {
		this.setLayout(new GridLayout());
		ArrayList<Product> products = Product.downloadAll();
		this.add(TableGenerator.generateTableWithScrollPane(products));
	}
	
	private DefaultComboBoxModel<String> getComboModel() {
		ArrayList<Product> products = Product.downloadAll();
		String[] productsName = new String[products.size()];
		for (int i = 0; i < products.size(); ++i) {
			productsName[i] = products.get(i).getName() + "#" + products.get(i).getId();
		}
		
		DefaultComboBoxModel<String> cbm = new DefaultComboBoxModel<String>(productsName);
		
		return cbm;
	}
}
