/**
 * 
 */
package ro.tuc.pt.tema3.dataAccessLayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author Vali
 *
 */
public class Database {
	private Connection dbHandle;
	
	private String dbhost;
	private String dbport;
	private String dbname;
	private String dbuser;
	private String dbpass;
	
	public Database() {
		File file = new File("src/resources/database.cfg");
		Scanner scan = null;
		try {
			scan = new Scanner(file);
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, "src/resources/database.cfg file is not found.", "Fatal error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		
		dbhost = (scan.nextLine()).substring(9);
		dbport = (scan.nextLine()).substring(9);
		dbname = (scan.nextLine()).substring(9);
		dbuser = (scan.nextLine()).substring(9);
		dbpass = (scan.nextLine()).substring(9);
		
		init();
	}
	
	/**
	 * Method for connecting to database using jdbc driver
	 */
	private void init() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		}
		catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Fatal error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}

		this.dbHandle = null;
		try {
			this.dbHandle = DriverManager.getConnection(
					"jdbc:mysql://" + dbhost + ":" + dbport + "/" + dbname + "?useSSL=false", 
					dbuser, dbpass
				);
		}
		catch (SQLException ex) {
			JOptionPane.showMessageDialog(
					null, 
					"SQL Connection failed: " + ex.getMessage(), 
					"Fatal error", 
					JOptionPane.ERROR_MESSAGE
				);
			System.exit(1);
		}
	}

	/**
	 * @return the dbHandle
	 */
	public Connection getDatabaseConnection() {
		return dbHandle;
	}

	/**
	 * @return the dbhost
	 */
	public String getDatabaseHost() {
		return dbhost;
	}

	/**
	 * @return the dbport
	 */
	public String getDatabasePort() {
		return dbport;
	}
	/**
	 * @return the dbname
	 */
	public String getDatabaseName() {
		return dbname;
	}

	/**
	 * @return the dbuser
	 */
	public String getDatabaseUsername() {
		return dbuser;
	}
	
	public void close() {
		try {
			this.dbHandle.close();
		} catch (SQLException e) { }
	}

	@Override
	public String toString() {
		return "Database [dbhost=" + dbhost + ", dbport=" + dbport + ", dbname=" + dbname + ", dbuser=" + dbuser + "]";
	}
	
	/**
	 * Method for executing a query
	 * @param sql the query to execute
	 * @return an ArrayList of the results as objects
	 */
	public static ArrayList< ArrayList<Object> > doQuery(String sql) {
		Database db = new Database();
		ArrayList< ArrayList<Object> > result = new ArrayList< ArrayList<Object> >();

		try {
			PreparedStatement ps = db.dbHandle.prepareStatement(sql);
			ResultSet rst = ps.executeQuery();
			ResultSetMetaData rsmd = rst.getMetaData();
			int columnsNo = rsmd.getColumnCount();
			
			while (rst.next()) {
				ArrayList<Object> line = new ArrayList<Object>();
				for (int index = 1; index <= columnsNo; ++index) {
					line.add(rst.getObject(index));
				}
				result.add(line);
			}
			
			rst.close();
			ps.close();
		} catch(SQLException e) {
			JOptionPane.showMessageDialog(
					null,
					"SQL Exception: " + e.getMessage(),
					"Fatal error", 
					JOptionPane.ERROR_MESSAGE
				);
			System.exit(1);
		}
		
		db.close();
		return result;
	}

	/**
	 * Method for executing an update
	 * @param sql the query to execute
	 * @return number of rows affected by the update
	 */
	public static int doUpdate(String sql) {
		Database db = new Database();
		int rowsAffected = 0;
		
		try {
			PreparedStatement ps = db.dbHandle.prepareStatement(sql);
			rowsAffected = ps.executeUpdate();	
			ps.close();
		} catch(SQLException e) {
			JOptionPane.showMessageDialog(
					null,
					"SQL Exception: " + e.getMessage(),
					"Fatal error", 
					JOptionPane.ERROR_MESSAGE
				);
			System.exit(1);
		}
		
		db.close();
		return rowsAffected;
	}

	/**
	 * Method for executing an insert
	 * @param sql the query to execute
	 * @return number of rows affected by the insert
	 */
	public static int doInsert(String sql) {
		return doUpdate(sql);
	}
	
	/**
	 * Method for executing a delete
	 * @param sql the query to execute
	 * @return number of rows affected by the delete
	 */
	public static int doDelete(String sql) {
		return doUpdate(sql);
	}
}
