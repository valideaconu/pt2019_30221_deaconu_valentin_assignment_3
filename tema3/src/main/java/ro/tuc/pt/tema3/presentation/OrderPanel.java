/**
 * 
 */
package ro.tuc.pt.tema3.presentation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import javafx.util.Pair;
import ro.tuc.pt.tema3.businessLayer.TableGenerator;
import ro.tuc.pt.tema3.model.Client;
import ro.tuc.pt.tema3.model.Product;

/**
 * @author Vali
 *
 */
public class OrderPanel extends JPanel {
	private static final long serialVersionUID = 8041590383470890433L;

	private JTable table;
	private JTextArea textArea;
	private ArrayList< Pair<Product, Integer> > productOnBill;
	
	private ArrayList<Paragraph> paragraphs;
	
	public OrderPanel() {
		this.setLayout(new BorderLayout());
		this.productOnBill = new ArrayList< Pair<Product, Integer> >();
		this.paragraphs = new ArrayList<Paragraph>();
		
		JPanel options = new JPanel();
		options.setLayout(new GridLayout(0, 3));
		
		JLabel clientLbl = new JLabel("Client", SwingConstants.CENTER);
		clientLbl.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		options.add(clientLbl);
		
		JLabel nameFilter = new JLabel("Name filter:", SwingConstants.CENTER);
		nameFilter.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		options.add(nameFilter);
		
		JLabel priceFilter = new JLabel("Minimum price:", SwingConstants.CENTER);
		priceFilter.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		options.add(priceFilter);
		
		
		final ArrayList<Client> clients = Client.downloadAll();
		String[] clientNames = new String[clients.size()];
		for (int i = 0; i < clients.size(); ++i)
			clientNames[i] = clients.get(i).getName() + "#" + clients.get(i).getId();
		
		final JComboBox<String> clientCombo = new JComboBox<String>(clientNames);
		clientCombo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				updateBill(clients.get(clientCombo.getSelectedIndex()));
			}
		});
		options.add(clientCombo);
		
		final JTextField nameTf = new JTextField();
		nameTf.setFont(new Font("Ubuntu", Font.PLAIN, 14));

		options.add(nameTf);
		
		final JSlider priceSlider = new JSlider(JSlider.HORIZONTAL, 0, 500, 0);
		priceSlider.setMajorTickSpacing(100);
		priceSlider.setMinorTickSpacing(50);
		priceSlider.setPaintTicks(true);
		priceSlider.setPaintLabels(true);
		priceSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				int minimumPrice = priceSlider.getValue();
				updateTable(nameTf.getText(), minimumPrice);
			}
		});
		options.add(priceSlider);

		nameTf.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent arg0) {
				updateTable(nameTf.getText(), priceSlider.getValue());
			}

			public void insertUpdate(DocumentEvent arg0) {
				updateTable(nameTf.getText(), priceSlider.getValue());				
			}

			public void removeUpdate(DocumentEvent arg0) {
				updateTable(nameTf.getText(), priceSlider.getValue());				
			}
			
		});
		
		this.add(options, BorderLayout.PAGE_START);
	
		JPanel center = new JPanel(new GridLayout(1, 2));
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				if (arg0.getClickCount() == 2) {
					int rowSelected = table.getSelectedRow();
					int itemId = (Integer) table.getValueAt(rowSelected, 0);
					
					Product p = Product.download(itemId);
					String input = JOptionPane.showInputDialog("Insert quantity: ");
					if (input != null && input.matches("[0-9]+")) {
						int quantityRequired = Integer.parseInt(input);
						if (p.getQuantity() >= quantityRequired) {
							p.setQuantity(p.getQuantity() - quantityRequired);
							if (Product.update(p)) {
								// success
								productOnBill.add(new Pair<Product, Integer>(p, quantityRequired));
								updateBill(clients.get(clientCombo.getSelectedIndex()));
								updateTable(nameTf.getText(), priceSlider.getValue());
							} else {
								// software failure
								JOptionPane.showMessageDialog(
										null, 
										"Something went wrong. Please try again later.", 
										"Fatal error",
										JOptionPane.ERROR_MESSAGE);
								System.exit(1);
							}
						} else {
							// under-stock
							JOptionPane.showMessageDialog(
									null, 
									"We have only " + p.getQuantity() + " product(s) on stock.", 
									"Error",
									JOptionPane.ERROR_MESSAGE);
						}
					} else {
						// invalid input
						JOptionPane.showMessageDialog(
								null, 
								"Invalid input.", 
								"Fatal error",
								JOptionPane.ERROR_MESSAGE);
						
					}
				}
			}			
		});
		table.setBorder(new MatteBorder(0, 0, 0, 2, new Color(64, 115, 255)));
		
		JScrollPane tableSP = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		center.add(tableSP);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setFont(new Font("Ubuntu", Font.PLAIN, 15));
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		JScrollPane textAreaSP = new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		center.add(textAreaSP);
		
		this.add(center);
		
		updateTable("", 0);
		updateBill(clients.get(0));
		
		JButton saveBill = new JButton("Generate Bill");
		saveBill.setHorizontalAlignment(SwingConstants.CENTER);
		saveBill.setFont(new Font("Ubuntu", Font.PLAIN, 16));
		saveBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				generateBill();
			}			
		});
		this.add(saveBill, BorderLayout.PAGE_END);
	}
	
	private void updateTable(String nameFilter, int minimumPrice) {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		
		ArrayList<Product> products = Product.downloadAllWithFilters(nameFilter, minimumPrice);
		JTable tableGen = TableGenerator.generateTable(products);
		if (tableGen != null)
			model = (DefaultTableModel) tableGen.getModel();
		else {
			model.setRowCount(0);
		}
		
		table.setModel(model);
	}
	
	private void updateBill(Client c) {
		paragraphs.clear();
		String bill = new String("");
		bill = "Best Warehouse Inc.\n";
		paragraphs.add(new Paragraph("Best Warehouse Inc."));
		bill += "Street Grigore Moisil 3, 400499\n";
		paragraphs.add(new Paragraph("Street Grigore Moisil 3, 400499"));
		bill += "\n";
		paragraphs.add(new Paragraph(""));
		bill += "Client: " + c.getName() + "\n";
		paragraphs.add(new Paragraph("Client: " + c.getName()));
		bill += "Adress: " + c.getAdress() + "\n";
		paragraphs.add(new Paragraph("Adress: " + c.getAdress()));
		bill += "Phone: " + c.getPhone() + "\n";
		paragraphs.add(new Paragraph("Phone: " + c.getPhone()));
		bill += "\n";
		paragraphs.add(new Paragraph(""));
		if (productOnBill.size() != 0) {
			bill += "Products: \n";
			paragraphs.add(new Paragraph("Products: "));
			double total = 0.0;
			DecimalFormat df = new DecimalFormat("#.00"); 
			for (Pair<Product, Integer> pair : productOnBill) {
				bill += pair.getKey().getName() + ", "
						+ "$" + df.format(pair.getKey().getPrice()) + " "
						+ "x" + df.format(pair.getValue()) + "\n";
				
				paragraphs.add(new Paragraph(pair.getKey().getName() + ", "
						+ "$" + df.format(pair.getKey().getPrice()) + " "
						+ "x" + df.format(pair.getValue())));
				
				total += pair.getKey().getPrice() * pair.getValue();
			}
			bill += "Total: $" + df.format(total) + "\n";
			paragraphs.add(new Paragraph("Total: $" + df.format(total)));
		}
		bill += "\n";
		paragraphs.add(new Paragraph(""));
		bill += "Thank you!\n";
		paragraphs.add(new Paragraph("Thank you!"));
		bill += "Have a nice day!\n";
		paragraphs.add(new Paragraph("Have a nice day!"));
		textArea.setText(bill);			
	}
	
	private void generateBill() {		
		DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm"); 
		String fileName = new String("Factura_" + dateFormat.format(new Date()) + ".pdf");
		
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(fileName));
		} catch (FileNotFoundException e) { 
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		} catch (DocumentException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		 
		document.open();
		com.itextpdf.text.Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
		 
		try {
			for (Paragraph p : paragraphs) {
				p.setFont(font);
				document.add(p);
			}
		} catch (DocumentException e) { 
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
		document.close();
	}
}
