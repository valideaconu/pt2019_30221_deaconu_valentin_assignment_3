/**
 * 
 */
package ro.tuc.pt.tema3.presentation;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import ro.tuc.pt.tema3.businessLayer.TableGenerator;
import ro.tuc.pt.tema3.model.Client;

/**
 * @author Vali
 *
 */
public class ClientPanel extends JPanel {
	private static final long serialVersionUID = 6547606762582495368L;

	public ClientPanel(int model) {
		switch (model) {
			case 0:
				// New client
				initNew();
				break;
			case 1:
				// Edit client
				initEdit();
				break;
			case 2:
				// Delete client
				initDelete();
				break;
			case 3:
			default:
				// View all clients
				initViewAll();
				break;
		}
	}
	
	private void initNew() {		
		JPanel inside = new JPanel();
		
		SpringLayout layout = new SpringLayout();
		inside.setLayout(layout);
		
		JLabel lblName = new JLabel("Name:", JLabel.TRAILING);
		lblName.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblName.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblName);
		
		final JTextField tfName = new JTextField(40);
		lblName.setLabelFor(tfName);
		tfName.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		inside.add(tfName);
		
		JLabel lblAdress = new JLabel("Adress:", JLabel.TRAILING);
		lblAdress.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblAdress.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblAdress);
		
		final JTextField tfAdress = new JTextField(40);
		lblAdress.setLabelFor(tfAdress);
		tfAdress.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		inside.add(tfAdress);
		
		JLabel lblPhone = new JLabel("Phone:", JLabel.TRAILING);
		lblPhone.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblPhone.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblPhone);
		
		final JTextField tfPhone = new JTextField(40);
		tfPhone.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblPhone.setLabelFor(tfPhone);
		inside.add(tfPhone);
		
		SpringUtilities.makeCompactGrid(inside, 3, 2, 40, 40, 80, 80);
		
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(Box.createVerticalGlue());
		this.add(inside);
		this.add(Box.createVerticalGlue());

		JButton btnSend = new JButton("Send");
		btnSend.setFont(new Font("Ubuntu", Font.PLAIN, 16));
		btnSend.setHorizontalAlignment(SwingConstants.CENTER);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = tfName.getText();
				String adress = tfAdress.getText();
				String phone = tfPhone.getText();
				
				Client c = new Client();
				if (c.setName(name) && c.setAdress(adress) && c.setPhone(phone)) {
					if (Client.insert(c)) {
						JOptionPane.showMessageDialog(
								null, 
								"Client added successfully.",
								"Success",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(
								null, 
								"Something went wrong. Try again later.",
								"Error",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(
							null, 
							"Invalid fields.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}			
		});
		this.add(btnSend);
		
		this.add(Box.createVerticalGlue());
		
	}
	
	private void initEdit() {		
		JPanel inside = new JPanel();
		
		SpringLayout layout = new SpringLayout();
		inside.setLayout(layout);
		
		final ArrayList<Client> clients = Client.downloadAll();
		final JComboBox<String> combo = new JComboBox<String>();
		combo.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		combo.setMaximumSize(new Dimension(400, 80));
		combo.setModel(getComboModel());
		
		JLabel lblName = new JLabel("Name:", JLabel.TRAILING);
		lblName.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblName.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblName);
		
		final JTextField tfName = new JTextField(40);
		lblName.setLabelFor(tfName);
		tfName.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		tfName.setText(clients.get(0).getName());
		inside.add(tfName);
		
		JLabel lblAdress = new JLabel("Adress:", JLabel.TRAILING);
		lblAdress.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblAdress.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblAdress);
		
		final JTextField tfAdress = new JTextField(40);
		lblAdress.setLabelFor(tfAdress);
		tfAdress.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		tfAdress.setText(clients.get(0).getAdress());
		inside.add(tfAdress);
		
		JLabel lblPhone = new JLabel("Phone:", JLabel.TRAILING);
		lblPhone.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		lblPhone.setHorizontalAlignment(SwingConstants.RIGHT);
		inside.add(lblPhone);
		
		final JTextField tfPhone = new JTextField(40);
		lblPhone.setLabelFor(tfPhone);
		tfPhone.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		tfPhone.setText(clients.get(0).getPhone());
		inside.add(tfPhone);
		
		SpringUtilities.makeCompactGrid(inside, 3, 2, 40, 40, 80, 80);
		
		combo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				int index = combo.getSelectedIndex();
				Client c = clients.get(index);
				tfName.setText(c.getName());
				tfAdress.setText(c.getAdress());
				tfPhone.setText(c.getPhone());
			}			
		});
		
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(Box.createVerticalGlue());
		this.add(combo);
		this.add(Box.createVerticalGlue());
		this.add(inside);
		this.add(Box.createVerticalGlue());

		JButton btnSend = new JButton("Send");
		btnSend.setFont(new Font("Ubuntu", Font.PLAIN, 16));
		btnSend.setHorizontalAlignment(SwingConstants.CENTER);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String name = tfName.getText();
				String adress = tfAdress.getText();
				String phone = tfPhone.getText();
				
				Client c = clients.get(combo.getSelectedIndex());
				if (c.setName(name) && c.setAdress(adress) && c.setPhone(phone)) {
					if (Client.update(c)) {
						JOptionPane.showMessageDialog(
								null, 
								"Client updated successfully.",
								"Success",
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(
								null, 
								"Something went wrong. Try again later.",
								"Error",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(
							null, 
							"Invalid fields.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}			
		});
		this.add(btnSend);
		
		this.add(Box.createVerticalGlue());
		
	}
	
	private void initDelete() {	
		JPanel inside = new JPanel();
		
		inside.setLayout(new FlowLayout());
		
		final JComboBox<String> combo = new JComboBox<String>();
		combo.setFont(new Font("Ubuntu", Font.PLAIN, 14));
		combo.setMaximumSize(new Dimension(400, 80));
		combo.setModel(getComboModel());
		
		inside.add(combo);
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.add(Box.createVerticalGlue());
		this.add(inside);
		this.add(Box.createVerticalGlue());

		JButton btnSend = new JButton("Send");
		btnSend.setFont(new Font("Ubuntu", Font.PLAIN, 16));
		btnSend.setHorizontalAlignment(SwingConstants.CENTER);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Client c = Client.downloadAll().get(combo.getSelectedIndex());
				if (Client.delete(c)) {
					JOptionPane.showMessageDialog(
							null, 
							"Client deleted successfully.",
							"Success",
							JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(
							null, 
							"Something went wrong. Try again later.",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
				
				combo.setModel(getComboModel());
				combo.setSelectedIndex(0);
			}			
		});
		
		JPanel inside2 = new JPanel(new FlowLayout());
		inside2.add(btnSend);
		this.add(inside2);
		
		this.add(Box.createVerticalGlue());
		
	}
	
	private void initViewAll() {
		this.setLayout(new GridLayout());
		ArrayList<Client> clients = Client.downloadAll();
		this.add(TableGenerator.generateTableWithScrollPane(clients));
	}
	
	private DefaultComboBoxModel<String> getComboModel() {
		ArrayList<Client> clients = Client.downloadAll();
		String[] clientsName = new String[clients.size()];
		for (int i = 0; i < clients.size(); ++i) {
			clientsName[i] = clients.get(i).getName() + "#" + clients.get(i).getId();
		}
		
		DefaultComboBoxModel<String> cbm = new DefaultComboBoxModel<String>(clientsName);
		
		return cbm;
	}
}
