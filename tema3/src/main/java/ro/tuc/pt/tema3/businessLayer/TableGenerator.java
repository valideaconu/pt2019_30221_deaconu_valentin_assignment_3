	/**
 * 
 */
package ro.tuc.pt.tema3.businessLayer;

import java.lang.reflect.*;

import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * @author Vali
 *
 */
public class TableGenerator {
	public static JTable generateTable(ArrayList<? extends Object> content) {
		if (content == null || content.size() == 0) {
			return null;
		}
		
		Field[] fields = content.get(0).getClass().getDeclaredFields();
		
		String[] titles = new String[fields.length];
		
		for (int i = 0; i < fields.length; ++i) {
			fields[i].setAccessible(true);			
			titles[i] = fields[i].getName();
		}
		
		Object[][] data = new Object[content.size()][fields.length];

		for (int k = 0; k < content.size(); ++k) {
			Object obj = content.get(k);
			for (int i = 0; i < fields.length; ++i) {
				fields[i].setAccessible(true);	
				try {
					Object value = fields[i].get(obj);
					data[k][i] = value;
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		
		JTable table = new JTable();
		DefaultTableModel tableModel = new DefaultTableModel() {
			private static final long serialVersionUID = 5003064870047274976L;

			@Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		tableModel.setColumnIdentifiers(titles);
		for (int i = 0; i < data.length; ++i)
			tableModel.insertRow(i, data[i]);
		
		table.setModel(tableModel);
		return table;
	}
	
	public static JScrollPane generateTableWithScrollPane(ArrayList<? extends Object> content) {
		JTable table = generateTable(content);
		JScrollPane sp = new JScrollPane(
				table, 
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
			);
		return sp;
	}
}
