/**
 * Product class used to interact with products table in database
 */
package ro.tuc.pt.tema3.model;

import java.util.ArrayList;

import ro.tuc.pt.tema3.dataAccessLayer.Database;

/**
 * @author Vali
 *
 */
public class Product {
	private int id;
	private String name;
	private String description;
	private double price;
	private int quantity;
	
	public Product(int id, String name, String description, double price, int quantity) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.quantity = quantity;
	}

	public Product() {	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 * @return true if operation succeeded, else false
	 */
	public boolean setName(String name) {
		if (name.matches("[a-zA-Z ]+")) {
			this.name = name;
			return true;
		}
		
		return false;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 * @return true if operation succeeded, else false
	 */
	public boolean setDescription(String description) {
		for (int i = 0; i < description.length(); ++i) {
			char c = description.charAt(i);
			if (!Character.isDigit(c) && !Character.isLetter(c) 
				&& !(c == ' ') && !(c == ',') && !(c == ':') && !(c == '.')) {
				return false;
			}
		}
		
		this.description = description;
		return true;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 * @return true if operation succeeded, else false
	 */
	public boolean setPrice(double price) {
		if (price < 0.0001)
			return false;

		this.price = price;
		return true;
	}
	
	/**
	 * @return the quantity
	 */
	public int getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 * @return true if operation succeeded, else false
	 */
	public boolean setQuantity(int quantity) {
		if (quantity < 0)
			return false;

		this.quantity = quantity;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name 
				+ ", description=" + description + ", price=" + price 
				+ ", qunatity=" + quantity + "]";
	}

	/**
	 * Method for extracting a product from database using its id
	 * @param id the id of product
	 * @return the product
	 */
	public static Product download(int id) {
		String sql = "SELECT name, description, price, quantity FROM products WHERE id = " + id + ";";
		ArrayList< ArrayList<Object> > results = Database.doQuery(sql);
		
		if (results != null) {
			if (results.size() == 1) {
				ArrayList<Object> res = results.get(0);
				
				return new Product(
						id,
						(String) res.get(0),
						(String) res.get(1),
						Double.parseDouble((String) res.get(2)),
						((Long) res.get(3)).intValue()
					);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	/**
	 * Method for uploading a new product to database
	 * @param p the product object
	 * @return true if operation succeeded, else false
	 */
	public static boolean insert(Product p) {
		String sql = "INSERT INTO products (name, description, price, quantity) VALUES ("
				+ "'" + p.name + "', '" + p.description + "', '" + p.price + "', '" + p.quantity + "');";
		
		if (Database.doInsert(sql) == 1)
			return true;
			
		return false;
	}

	/**
	 * Method for updating an existing product on database using his id
	 * @param p the product object
	 * @return true if operation succeeded, else false
	 */
	public static boolean update(Product p) {
		String sql = "UPDATE products "
				+ "SET name = '" + p.name + "', description = '" + p.description 
				+ "', price = '" + p.price + "', quantity = '" + p.quantity + "' "
				+ "WHERE id = '" + p.id + "';";
		
		if (Database.doUpdate(sql) == 1) 
			return true;
		return false;
	}

	/**
	 * Method for deleting an existing product on database using his id
	 * @param p the product object
	 * @return true if operation succeeded, else false
	 */
	public static boolean delete(Product p) {
		String sql = "DELETE FROM products WHERE id = '" + p.id + "';";
		
		if (Database.doUpdate(sql) == 1)
			return true;
		
		return false;
	}
	
	/**
	 * Method for extracting all products from database
	 * @return a list with all products
	 */
	public static ArrayList<Product> downloadAll() {
		String sql = "SELECT id, name, description, price, quantity FROM products;";
		ArrayList< ArrayList<Object> > results = Database.doQuery(sql);
		
		if (results != null) {		
			ArrayList<Product> products = new ArrayList<Product>();
			
			for(ArrayList<Object> arr : results) {
				products.add(new Product(
						((Long) arr.get(0)).intValue(),
						(String) arr.get(1),
						(String) arr.get(2),
						Double.parseDouble((String) arr.get(3)),
						((Long) arr.get(4)).intValue()
					));
			}
			
			return products;
		} else {
			return null;
		}
	}
	
	/**
	 * Method for extracting all products from database based on filters
	 * @param nameFilter a string with filters for product name
	 * @param priceMin a minimum price filter
	 * @return a list with all products filtered by filters
	 */
	public static ArrayList<Product> downloadAllWithFilters(String nameFilter, int priceMin) {
		String sql = "SELECT id, name, description, price, quantity FROM products "
				+ "WHERE name LIKE '%" + nameFilter + "%' and price >= '" + priceMin + "';";
		ArrayList< ArrayList<Object> > results = Database.doQuery(sql);
		
		if (results != null) {		
			ArrayList<Product> products = new ArrayList<Product>();
			
			for(ArrayList<Object> arr : results) {
				products.add(new Product(
						((Long) arr.get(0)).intValue(),
						(String) arr.get(1),
						(String) arr.get(2),
						Double.parseDouble((String) arr.get(3)),
						((Long) arr.get(4)).intValue()
					));
			}
			
			return products;
		} else {
			return null;
		}
	}
}
